package personal.carlosjgr7.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;

public class NotificationHandler extends ContextWrapper {

    private NotificationManager notificationManager;
    private static final String Chanel_Low_Name ="LOW CHANEL";
    public static final String Chanel_Low_ID ="1";
    private final String Chanel_High_Name ="HIGH CHANEL";
    public static final String Chanel_HIGH_ID ="2";
    private final String Chanel_Group_notificacion ="Group Notification";
    public static final int Chanel_Group_id =100;



    public NotificationHandler(Context context) {
        super(context);
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel LowChanel = new NotificationChannel(
                    Chanel_Low_ID, Chanel_Low_Name, NotificationManager.IMPORTANCE_LOW);


            NotificationChannel HighChannel = new NotificationChannel(
                    Chanel_HIGH_ID, Chanel_High_Name, NotificationManager.IMPORTANCE_HIGH);


            getManager().createNotificationChannel(LowChanel);
            getManager().createNotificationChannel(HighChannel);
        }
    }


    public NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }


    public Notification.Builder CreateNotification(String title, String body, String canal) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            return getAndroidChannelNotification(title, body,canal);

        return getAndroidNotification(title, body);
    }


    private Notification.Builder getAndroidChannelNotification(String title, String body,String chanelId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(this,DetailActivity.class);
            intent.putExtra("title",title);
            intent.putExtra("content",body);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pintent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_CANCEL_CURRENT);

                return new Notification.Builder(getApplicationContext(), chanelId)
                        .setContentTitle(title)
                        .setContentIntent(pintent)
                        .setContentText(body)
                        .setGroup(Chanel_Group_notificacion)
                        .setSmallIcon(android.R.drawable.stat_notify_more)
                        .setAutoCancel(true);
        }
        return null;
    }

    private Notification.Builder getAndroidNotification(String title, String body) {
            return new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(android.R.drawable.stat_notify_more)
                    .setAutoCancel(true);
    }


    public Notification.Builder SumaryNotification(String chanelId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification SumaryNotification = new Notification.Builder(getApplicationContext(), chanelId)
                    .setGroup(Chanel_Group_notificacion)
                    .setGroupSummary(true)
                    .setSmallIcon(android.R.drawable.star_big_on)
                    .setAutoCancel(true)
                    .build();
            getManager().notify(Chanel_Group_id,SumaryNotification);
        }
        return null;
    }
}

