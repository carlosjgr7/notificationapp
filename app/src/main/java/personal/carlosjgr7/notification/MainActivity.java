package personal.carlosjgr7.notification;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static personal.carlosjgr7.notification.NotificationHandler.Chanel_HIGH_ID;
import static personal.carlosjgr7.notification.NotificationHandler.Chanel_Low_ID;

public class MainActivity extends AppCompatActivity {
    NotificationHandler notificationHandler;
    int count =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        notificationHandler = new NotificationHandler(this);


    }

    @OnClick(R.id.btnImportantHigh)
    public void onClickHigh() {

        Notification.Builder nb = notificationHandler.CreateNotification(
                "Titulo","descripcion de la notificacion High",Chanel_HIGH_ID);
        notificationHandler.SumaryNotification(Chanel_HIGH_ID);
        notificationHandler.getManager().notify(++count,nb.build());


    }


    @OnClick(R.id.btnImportantLow)
    public void onClickLow() {
        Notification.Builder nb = notificationHandler.CreateNotification(
                "Titulo","descripcion",Chanel_Low_ID);
        notificationHandler.getManager().notify(++count,nb.build());

    }

}
