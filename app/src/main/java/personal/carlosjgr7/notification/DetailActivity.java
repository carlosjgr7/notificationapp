package personal.carlosjgr7.notification;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.tbContent)
    TextView textViewContent;
    @BindView(R.id.tbTitle)
    TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setValues();
    }

    private void setValues() {
        textViewContent.setText(getIntent().getStringExtra("content"));
        textViewTitle.setText(getIntent().getStringExtra("title"));
    }
}
